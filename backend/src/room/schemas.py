from pydantic import BaseModel


class BaseRoom(BaseModel):
    name: str


class RoomCreate(BaseRoom):
    pass


class Room(BaseRoom):
    id: int
