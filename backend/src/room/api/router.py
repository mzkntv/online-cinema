from fastapi import APIRouter, status, WebSocket, WebSocketDisconnect

from .. import schemas

router = APIRouter(prefix="/rooms", tags=["room"])


@router.get("/", response_model=list[schemas.Room])
def get_rooms():
    return [
        schemas.Room(id=1, name="test room 1"),
        schemas.Room(id=2, name="test room 2"),
        schemas.Room(id=3, name="test room 3"),
    ]


@router.get("/{room_id}/", response_model=schemas.Room)
def get_room(room_id: int):
    return schemas.Room(id=room_id, name="test room")


@router.post("/", status_code=status.HTTP_201_CREATED, response_model=schemas.Room)
def create_room(room: schemas.RoomCreate):
    return schemas.Room(id=1, name=room.name)
