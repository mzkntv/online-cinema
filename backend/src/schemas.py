from datetime import timedelta
from enum import Enum

from pydantic import BaseModel, HttpUrl
from pydantic.json import timedelta_isoformat


class CommandEnum(str, Enum):
    play = "play"
    pause = "pause"
    set_url = "set_url"
    welcome = "welcome"


class Command(BaseModel):
    """
    Команда для отправки клиенту
    """

    type: CommandEnum
    url: HttpUrl | None
    is_playing: bool | None
    playback_ratio: float | None
    current_time: timedelta | None

    class Config:
        json_encoders = {
            timedelta: timedelta_isoformat,
        }

    class Meta:
        use_enum_values = True
