from __future__ import annotations

import abc
import dataclasses
from datetime import timedelta, datetime
from typing import Callable

from fastapi import FastAPI, WebSocket, WebSocketDisconnect
from pydantic import HttpUrl

from .room import api
from . import schemas

app = FastAPI(root_path="/api")
app.include_router(api.router)


VIDEOPLAYER_COMMANDS: dict[str, Callable[[VideoPlayer], None]] = {
    "play": lambda player: player.play(),
    "pause": lambda player: player.play(),
    "set_media": lambda player, media_url: player.change_media(media_url),
}


@dataclasses.dataclass
class Member(abc.ABC):
    @abc.abstractmethod
    def send_command(self) -> None:
        raise NotImplementedError


@dataclasses.dataclass
class VideoPlayer:
    media_url: HttpUrl | None = None
    is_playing: bool = False
    playback_ratio: float = 1
    _start_playing_at: datetime | None = None
    _accumulated_playback_time: timedelta = dataclasses.field(default_factory=timedelta)

    @property
    def current_time(self) -> timedelta:
        if not self.is_playing:
            return self._accumulated_playback_time
        return self._accumulated_playback_time + (
            (datetime.now() - self._start_playing_at) * self.playback_ratio
        )

    def play(self) -> None:
        if self.is_playing:
            return
        self.is_playing = True
        self._start_playing_at = datetime.now()

    def pause(self) -> None:
        if not self.is_playing:
            return
        self.is_playing = False
        self._accumulated_playback_time += (
            datetime.now() - self._start_playing_at
        ) * self.playback_ratio

    def _reset(self) -> None:
        self.is_playing = False
        self._accumulated_playback_time = timedelta()
        self._start_playing_at = None

    def stop(self) -> None:
        self._reset()

    def change_media(self, media_url: HttpUrl) -> None:
        self._reset()
        self.media_url = media_url


@dataclasses.dataclass
class Room:
    player: VideoPlayer
    members: list[Member] = dataclasses.field(default_factory=list)


class ConnectionManager:
    def __init__(self, room: Room) -> None:
        self.active_connections: list[WebSocket] = []
        self.room = room

    async def connect(self, websocket: WebSocket) -> None:
        await websocket.accept()
        self.active_connections.append(websocket)
        self.room.members.append(websocket)
        await self.send_personal_command(
            schemas.Command(
                type=schemas.CommandEnum.welcome,
                url=self.room.player.media_url,
                is_playing=self.room.player.is_playing,
                playback_ratio=self.room.player.playback_ratio,
                current_time=self.room.player.current_time,
            ),
            websocket,
        )

    def disconnect(self, websocket: WebSocket) -> None:
        self.active_connections.remove(websocket)

    async def send_personal_command(
        self, command: schemas.Command, websocket: WebSocket
    ) -> None:
        await websocket.send_text(command.json(exclude_unset=True))

    async def send_personal_message(self, message: str, websocket: WebSocket) -> None:
        await websocket.send_text(message)

    async def broadcast(self, message: str) -> None:
        for connection in self.active_connections:
            await connection.send_text(message)

    async def broadcast_command_excluding_initiator(
        self, command: schemas.Command, initiator: WebSocket
    ) -> None:
        for connection in self.active_connections:
            if connection == initiator:
                continue
            await connection.send_json(command.dict(exclude_unset=True))


manager = ConnectionManager(
    Room(
        VideoPlayer(
            media_url="https://www.youtube.com/watch?v=AzsO67rloQw&list=PLvTBThJr861yMBhpKafII3HZLAYujuNWw&index=19"
        )
    )
)


@app.get("/")
def ping():
    return {"ping": "pong"}


@app.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket):
    await manager.connect(websocket)
    try:
        while True:
            data = await websocket.receive_json()
            command_payload = schemas.Command.parse_obj(data)
            command = VIDEOPLAYER_COMMANDS[command_payload.type]
            command(manager.room.player)
            await manager.broadcast_command_excluding_initiator(
                command=command_payload, initiator=websocket
            )
    except WebSocketDisconnect:
        manager.disconnect(websocket)
        await manager.broadcast("Client left the chat")
