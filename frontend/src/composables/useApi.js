import { ref } from 'vue';

export function useApi(getResults) {
  const error = ref(false);
  const loading = ref(false);
  const result = ref('');

  const callApi = () => {
    error.value = false;
    loading.value = true;

    getResults()
      .then((value) => {
        result.value = value;
      })
      .catch(() => {
        error.value = true;
      })
      .finally(() => {
        loading.value = false;
      });
  };

  return { error, loading, result, callApi };
}
