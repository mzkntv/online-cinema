import { createApp } from 'vue';
import { Quasar } from 'quasar';
import quasarLang from 'quasar/lang/ru';
import quasarIconSet from 'quasar/icon-set/eva-icons';

import 'video.js/dist/video-js.css';

import 'eva-icons/style/eva-icons.css';

import 'quasar/src/css/index.sass';

import App from './App.vue';
import { router } from './router';

createApp(App)
  .use(router)
  .use(Quasar, {
    plugins: {},
    lang: quasarLang,
    iconSet: quasarIconSet,
  })
  .mount('#app');
