import { createRouter, createWebHistory } from 'vue-router';
import Home from './pages/Home.vue';
import SimpleRouterView from './components/SimpleRouterView.vue';
import NotFound from './pages/NotFound.vue';

const routes = [
  {
    path: '/',
    component: Home,
    children: [
      {
        path: '',
        name: 'home',
        component: () => import('./pages/LandingPage.vue'),
      },
      {
        path: 'rooms',
        component: SimpleRouterView,
        children: [
          {
            path: '',
            name: 'rooms-list',
            component: () => import('./pages/Rooms.vue'),
          },
          {
            path: ':id',
            name: 'rooms-detail',
            props: (route) => ({ roomId: parseInt(route.params.id) }),
            component: () => import('./pages/RoomDetail.vue'),
          },
        ],
      },
    ],
  },
  { path: '/:pathMatch(.*)*', name: 'NotFound', component: NotFound },
];

export const router = createRouter({
  history: createWebHistory(),
  routes,
});
