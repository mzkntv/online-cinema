const socket = new WebSocket('ws://localhost:8000/ws');

socket.addEventListener('message', (e) => {
  const { type } = JSON.parse(e.data);
  console.log(type);
});

export function sendToWebSocket(message) {
  const stringifiedMessage = JSON.stringify(message);

  if (socket.readyState === WebSocket.OPEN) {
    socket.send(stringifiedMessage);
    return;
  }

  socket.addEventListener(
    'open',
    () => {
      socket.send(stringifiedMessage);
    },
    { once: true },
  );
}
